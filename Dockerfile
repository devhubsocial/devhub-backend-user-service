FROM openjdk:17-oracle
ENV APP_FILE=devhub-backend-user-service-0.0.1-SNAPSHOT.jar
ENV APP_HOME /usr/app
EXPOSE 8080
COPY . build/libs/devhub-backend-user-service-0.0.1-SNAPSHOT.jar $APP_HOME/
WORKDIR $APP_HOME
ENTRYPOINT ["sh", "-c"]
CMD ["exec java -jar $APP_FILE"]