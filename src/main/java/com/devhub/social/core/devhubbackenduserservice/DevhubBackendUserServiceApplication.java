package com.devhub.social.core.devhubbackenduserservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevhubBackendUserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DevhubBackendUserServiceApplication.class, args);
    }

}
